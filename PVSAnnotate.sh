#!/usr/bin/env bash
########### PVSAnnotate ############
#### 6/28/16 BSG, MBad, KS, JTD ####
####################################

input_vcf_file=$1 ### vcf file to annotate

filename=$(basename $input_vcf_file)
extension="${filename##*.}"
fnameout="${filename%.*}"

# echo "filename is $filename"
# echo "extension is $extension"
# echo "fnameout is $fnameout"

# rsid_vcf_out="${input_vcf_file}_rsid" ### interscting rsid file staging file output name
# rest_vcf_out="${input_vcf_file}_rest" ### remaining positions file staging file output name

rsid_vcf_out="${fnameout}_rsid" ### interscting rsid file staging file output name
rest_vcf_out="${fnameout}_rest" ### remaining positions file staging file output name

echo "PVSAnnotate running $input_vcf_file: Pre-processing..."

# fnameout=$(echo $input_vcf_file | cut -d'.vcf' -f1)

### 1 filter PVS relevant SNPs into temporary vcf file to be loaded into Rscript
vcftools --vcf $1 --snps rsidlist.tsv --recode --recode-INFO-all --out $rsid_vcf_out >> "${fnameout}_logfile.txt" 2>&1

echo "Done filtering relevant SNPs."

### 2 filter the remaining SNPs into staging final output file
vcftools --vcf $input_vcf_file --exclude rsidlist.tsv --recode --recode-INFO-all --out $rest_vcf_out >> "${fnameout}_logfile.txt" 2>&1

mv "${rsid_vcf_out}.recode.vcf" "${rsid_vcf_out}_recode_vcf"
mv "${rest_vcf_out}.recode.vcf" "${rest_vcf_out}_recode_vcf"

echo "Done filtering remaining SNPs."
echo "Now annotating VCF..."

### 3 run Rscript using curated vcf (step #1) // annotating vcf
rsid_vcf_to_run="${rsid_vcf_out}_recode_vcf"

echo "Now running R Script..."

Rscript PVSAnnotate.R $rsid_vcf_to_run

echo "Done running R Script."

### 4 Adding PVS=NA to remaining sites

rest_vcf_to_run="${rest_vcf_out}_recode_vcf"

grep -v '^#' $rest_vcf_to_run > "${input_vcf_file}_rest_noheader.vcf" # obtaining remaining sites w/o header

if [ "$(uname)" == "Darwin" ]; then
  gawk -F'\t' -vOFS='\t' '{$8=$8 ";PVS=NA"}1' "${input_vcf_file}_rest_noheader.vcf" > "${input_vcf_file}_rest_noheader_annotated.vcf" # adding in PVS annotation (NA)
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  awk -F'\t' -vOFS='\t' '{$8=$8 ";PVS=NA"}1' "${input_vcf_file}_rest_noheader.vcf" > "${input_vcf_file}_rest_noheader_annotated.vcf" # adding in PVS annotation (NA)
fi

### 5 Preparing final vcf output
echo "VCF annotation complete. Packaging files..."

grep "#" "${rest_vcf_out}_recode_vcf" > "${input_vcf_file}_PVS_pre.vcf" # create header within final file

if [ "$(uname)" == "Darwin" ]; then
  gsed '/^##INFO/!b;:a;n;//ba;i\##INFO=<ID=PVS,Number=.,Type=Float,Description="Pleiotropic Variant Score">' "${input_vcf_file}_PVS_pre.vcf" > "${fnameout}_PVS.vcf" # add INFO description to header
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  sed '/^##INFO/!b;:a;n;//ba;i\##INFO=<ID=PVS,Number=.,Type=Float,Description="Pleiotropic Variant Score">' "${input_vcf_file}_PVS_pre.vcf" > "${fnameout}_PVS.vcf" # add INFO description to header
fi

### 6 Combining both annotation files (annotated & NA)
cat *.vcf_pvsannotated >>"${input_vcf_file}_rest_noheader_annotated.vcf"

sort -k1,1 -k2,2n < "${input_vcf_file}_rest_noheader_annotated.vcf" > "${input_vcf_file}_combined_annotated.vcf" # sorting into original vcf order

cat "${input_vcf_file}_combined_annotated.vcf" >>"${fnameout}_PVS.vcf" # adding final, ordered vcf to complete header file

### Deleting various intermediate files
echo "PVSAnnotate complete. Cleaning up..."

rm -f "${input_vcf_file}_"*
rm -f *_recode_*

### Organizing final output files
mkdir -p Output

mv ${fnameout}_PVS.vcf, ${fnameout}_logfile.txt Output

echo "Done."
