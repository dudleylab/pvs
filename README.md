# PVS: Pleiotropic Variability Score #

This package is meant to complement the following publication. 

> Khader, S., *et al.*
> Pleiotropic Variability Score: Quantifying Phenomic Associations of Genetic Variants.
> Submitted

The goal of PVS is to... [MANUSCRIPT ABSTRACT HERE]
* * *  

# Requirements #

* Linux or OSX
* R (Version 3.XX)
* Vcftools (http://vcftools.sourceforge.net/)

# Usage #

## How do I get set up? ##

* Download PVS package from [here](https://bitbucket.org/dudleylab/pvs/)
* Put vcf to be annotated file into PVS directory
* Run: PVSAnnotate.sh _filename_.vcf
* PVS-annotated vcf can be found in the /Output/ folder as _filename_\_PVS.vcf

## Input ##

* vcf file in hg19 format

## Output ##

* vcf file with extra column containing PVS score
> \Output\\_filename_\_PVS.vcf

## Example ##

[Illumina](http://www.illumina.com/platinumgenomes/) provides sample whole-genome sequencing files for select individuals which can serve as a sample input for PVS.

1. Go to Illumina's [FTP](ftp://ussd-ftp.illumina.com/) website.
2. Go through the following path:
> /hg19/8.0.1/NA12878/ 
3. Download the zipped vcf file "NA12878.vcf.gz" into PVS directory.
4. Unzip vcf file (i.e. gunzip NA12878.vcf.gz)
5. Run XXXXX.R NA12878.vcf  
6. Analyze PVS-added vcf file in /Output folder, named "NA12878_PVS.vcf

* * *  

## Method: PVS calculation ##

**Note:** Due to file size constraints, we do not include all sources used to calculate PVS, but all files can be downloaded via the "External Datasets" section below. This section is meant to describe PVS methodology and is not required to run the program.  

1. Download Semantic Measures Library  
2. Download ontology databases (HPO/DO)  
3. Gather genotype-phenotype data (i.e. GWASdb)  
4. Map variants to phenotypes from curated database or manually.
5. Assign genotype-phenotype data and compute semantic similarity  
Libraries like SML offers multiple ontology-based semantic similarity assessments. 
We calculated semantic similarities across 172 metrics and picked six methods that provide control over bias toward longer lists having higher scores. Further, we selected two parameters each that provided approximations over Min, Max and Average semantic similarities  
6. We define pleiotropy as the inverse of similarity score gathered using semantic measures  
7. To derive the final score we have taken the average over the six metrics and calculated the inverse. Average PVS scores from HPO and DO (PVS-HPO and PVS-DO) were defined as the 
Pleiotropic Variability Score  

**Note:** We include the original scripts/files used to calculate PVS within the info/ directory.  

* * *

### External Datasets ###

**Note:** The following external datasets were utilized to calculate PVS score using the available, most up-to-date versions. We will release updates to PVS in scheduled intervals that will replace any out of date file versions.

**Human Phenotype Ontology (HPO)**  

* [HPO](http://human-phenotype-ontology.github.io/) was used to annotate phenotypes  
* Current Version Used: v1.2  
* Source: http://human-phenotype-ontology.github.io/downloads.html  
  
**Disease Ontology (DO)**  

* [DO](http://disease-ontology.org) was used to annotate phenotypes
* Current Version Used: v1.2
* Source: http://disease-ontology.org/downloads/  

**Note:** Alternate download sources for HPO/DO include [OBO](http://www.obofoundry.org/) & [Biofoundry/BioPortal](http://bioportal.bioontology.org/)  

**Semantic Measures Library & ToolKit**  

* [SML-Toolkit](http://www.semantic-measures-library.org/sml/) was used to calculate semantic similarity scores between phenotypes    
* Current Version Used: 0.9  
* Source: http://www.semantic-measures-library.org/sml/index.php?q=downloads#toolkit  


**GWASdb**  

* [GWASdb](http://jjwanglab.org/gwasdb) was used to map DO & HPO terms to rsids.
* Current Version Used: SEP 2014  
* Source: http://jjwanglab.org/gwasdb  

**Note:** GWASdb contains data from [GWAS catalog](https://www.genome.gov/26525384) and [GRASP](http://grasp.nhlbi.nih.gov/Overview.aspx) which were originally included separately  
 
* * *

## Contact Us ##

For any questions, issues, or comments of any kind, please contact us at:  

marcus.badgeley@mssm.edu  
Principal Investigator: Joel Dudley, Ph.D. | joel.dudley@mssm.edu  
**Dudley Lab** | http://dudleylab.org/